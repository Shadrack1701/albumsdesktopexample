package com.app.trax.album;

import com.app.trax.album.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Main application class, wires all the application beans and spins up local TomCat container on execution.
 */
@SpringBootApplication
public class AlbumApplication implements ApplicationRunner {

    @Autowired
    private ConfigurableApplicationContext context;

    private final AlbumService albumService;

    public AlbumApplication(AlbumService albumService) {
        this.albumService = albumService;
    }

    public static void main(String[] args) {
        SpringApplication.run(AlbumApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments arguments) {
        //Log output of application
        System.out.println(albumService.getPhotos(arguments));

        //When running via IDE this will cause it to look like it failed with a closing error,
        // it still worked if output was logged and/or file was generated
        SpringApplication.exit(context);
    }


}

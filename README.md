## What is this repository for? ##
* This is an example project for a command line appliction which gets static json content via rest call

## How do I get set up? ##
* Clone the project using [Source Tree](https://www.sourcetreeapp.com/), [GitKraken](https://www.gitkraken.com/) or [Git Bash](https://git-scm.com/downloads). 
* Have installed a Java IDE, I recommend [Eclipse](https://www.eclipse.org/downloads/) or [IntelliJ](https://www.jetbrains.com/idea/download/#section=windows)
* Use at least [Java 1.8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* This project is fully Mavenized, import as Maven project and dependencies should be managed for you.

## How to Run ##
#### After the project has been correctly imported as a maven project you then: ####
* Run the maven command "clean package" to generate the executable jar.
* once the jar is generated use a terminal to run the following example command:
    * java -jar c:/projects/albumsdesktopexample/target/PhotoRunner.jar --fileurl=c:/example/sample.json --albumid=10 --terminalout=50
    * Make sure you navigate to the directory with the jar, or specify the directory before the command as shown above. 
    * You can use any/all the following command line options in this manner:

 parameter      | parameter_description                                                                                        | default                                     | example 
--------------- | --------------------------------------------------------------------------------------------------------------------- | ------------------------------------------- | -----------------------------------------------------
photoserviceurl | Overrides the default url of the photo service.                                                                       | https://jsonplaceholder.typicode.com/photos | https://jsonplaceholder.typicode.com/photos?albumId=1
fileurl         | Specifies the output file which will be used if the output is larger than the specified terminalOut.                  | c:/PhotoRunnerOutput/output.json            | c:/PhotoRunnerOutput/example.json
terminalout     | Specifies the maximum output records to be returned to the console.  Additional records written to specified fileurl. | 100                                         | 223
albumid         | Filters the results by the specified album id                                                                         |                                             | 17
photoid         | Filters the results by the specified photo id                                                                         |                                             | 4985
title           | Filters the results by the specified photo title                                                                      |                                             | repudiandae iusto deleniti rerum
photourl        | Filters the results by the specified photo url                                                                        |                                             | https://via.placeholder.com/600/b3c89e
thumburl        | Filters the results by the specified photo thumbnail url                                                              |                                             | https://via.placeholder.com/150/c9c3b6       

##Making Changes ##
* The app has a suite of tests which should let you know any functionality has been affected by your changes.
* To ensure your code format and syntax is correct after your changes are complete run the maven command: clean package checkstyle:check pmd:check -Dpmd.printFailingErrors=true
    * This will check for any obvious syntax issues, unused code etc. 

package com.app.trax.album.service;

import com.app.trax.album.global.GlobalConstants;
import org.junit.Test;
import org.springframework.boot.DefaultApplicationArguments;


import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class AlbumServiceTests {

    private static final String SERVICE_PARAM = "--photoserviceurl=https://jsonplaceholder.typicode.com/photos";
    private static final String FILE_PARAM = "--fileurl=c:/PhotoRunnerOutput/output.json";
    private static final String TERMINAL_PARAM = "--terminalout=100";

    @Test
    public void testAlbumService() {
        AlbumService albumService = new AlbumService();
        String response = albumService.getPhotos(new DefaultApplicationArguments(new String[]{SERVICE_PARAM, FILE_PARAM, TERMINAL_PARAM}));
        assertTrue(response.contains("[1] accusamus beatae ad facilis cum similique qui sunt"));
        assertTrue(response.contains("[100]"));
        assertFalse(response.contains("[101]"));
    }

    @Test
    public void testEmptyResponse() {
        AlbumService albumService = new AlbumService();
        String response = albumService.getPhotos(new DefaultApplicationArguments(new String[]{SERVICE_PARAM, FILE_PARAM, TERMINAL_PARAM, "--albumid=31337"}));
        assertEquals("No photos were found for the given search: https://jsonplaceholder.typicode.com/photos?albumId=31337", response);
    }

    @Test(expected = InternalError.class)
    public void invalidFileUrlThrowsInternalError() {
        AlbumService albumService = new AlbumService();
        String[] args = new String[]{SERVICE_PARAM, TERMINAL_PARAM, "--fileurl=DISKDRYVE:/legit windows F0l|)3R"};
        albumService.getPhotos(new DefaultApplicationArguments(args));
    }

    @Test(expected = InternalError.class)
    public void invalidServiceUrlThrowsInternalError() {
        AlbumService albumService = new AlbumService();
        albumService.getPhotos(new DefaultApplicationArguments(new String[]{"--photoserviceurl=https:||jsonplaceholder.typicode.com/photos", FILE_PARAM, TERMINAL_PARAM}));
    }

    @Test(expected = InternalError.class)
    public void invalidServiceResponseThrowsInternalError() {
        AlbumService albumService = new AlbumService();
        albumService.getPhotos(new DefaultApplicationArguments(new String[]{"--photoserviceurl=https://httpbin.org/get", FILE_PARAM, TERMINAL_PARAM}));
    }

    @Test
    public void appDefaultsSetCorrectly() {
        AlbumService albumService = new AlbumService();
        albumService.getPhotos(new DefaultApplicationArguments(new String[]{"--albumid=31337"}));
        assertEquals(GlobalConstants.TERMINAL_OUT_DEFAULT, albumService.getTerminalOut());
        assertEquals(GlobalConstants.FILE_URL_DEFAULT, albumService.getFileUrl());
        assertEquals(GlobalConstants.PHOTO_SERVICE_DEFAULT, albumService.getPhotosUrl());
    }

    @Test
    public void invalidParametersAreIgnored() {
        AlbumService albumService = new AlbumService();
        String response = albumService.getPhotos(new DefaultApplicationArguments(new String[]{SERVICE_PARAM, "--albumid=31337", "--fakeparameter=false", "invalid--parameter--=false"}));
        assertEquals("No photos were found for the given search: https://jsonplaceholder.typicode.com/photos?albumId=31337", response);
    }

    @Test
    public void returnLessThanTerminalAmount() {
        AlbumService albumService = new AlbumService();
        String response = albumService.getPhotos(new DefaultApplicationArguments(new String[]{SERVICE_PARAM, "--albumid=1", "--photoid=40"}));
        assertFalse(response.contains("[39]"));
        assertTrue(response.contains("[40]"));
        assertFalse(response.contains("[41]"));
    }

    @Test
    public void mixedCaseCommandLineParamsWork() {
        AlbumService albumService = new AlbumService();
        String response = albumService.getPhotos(new DefaultApplicationArguments(new String[]{SERVICE_PARAM, "--AlBumID=1", "--PHOTOID=20"}));
        assertFalse(response.contains("[19]"));
        assertTrue(response.contains("[20]"));
        assertFalse(response.contains("[21]"));
    }

}
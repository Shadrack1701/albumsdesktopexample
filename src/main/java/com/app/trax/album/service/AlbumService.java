package com.app.trax.album.service;

import com.app.trax.album.dto.Photo;
import com.app.trax.album.global.GlobalConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


/**
 Created by mtrachsel on 5/22/2018.
 Purpose:  Calls the photo service based on the supplied filter criteria and returns the correct response via console
 or output file based on input configurations.
 */
@Service
public class AlbumService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlbumService.class);

    //All of the @Value variables get set by Spring based off of the application.properties or overridden by what is
    // parmed in via command line
    @Value("${photoserviceurl}")
    private String photosUrl;
    @Value("${albumid")
    private String albumId;
    @Value("${photoid}")
    private String photoId;
    @Value("${title}")
    private String title;
    @Value("${url}")
    private String photoUrl;
    @Value("${thumburl}")
    private String thumbUrl;
    @Value("${fileurl}")
    private String fileUrl;
    @Value("${terminalout}")
    private int terminalOut;

    private String filteredUrl = "";
    private MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
    private ObjectMapper objectMapper = new ObjectMapper();
    private RestTemplate restTemplate = new RestTemplate();

    /**
     This is the primary method which validates the request, then calls the photo service and returns the output back
     to the console or output file.

     @param arguments command line input parameters
     @return console output String
     */
    public String getPhotos(ApplicationArguments arguments) {
        validateRequest(arguments);
        return callPhotoService();
    }

    /**
     This method manages the input populating a parameter map used later to generate the service queryString.

     @param args passed in parameters
     */
    private void validateRequest(ApplicationArguments args) {
        LOGGER.debug("Application started with command-line arguments: {}", Arrays.toString(args.getSourceArgs()));
        LOGGER.debug("NonOptionArgs: {}", args.getNonOptionArgs());
        LOGGER.debug("OptionNames: {}", args.getOptionNames());

        for (String name : args.getOptionNames()) {
            LOGGER.debug("arg-{}={}", name, args.getOptionValues(name));
            if (!GlobalConstants.PhotoParameters.isValidPhotoParameters(name)) {
                LOGGER.info("Supplied argument is invalid {}={}, ignoring.", name, args.getOptionValues(name));
            } else {
                parameters.put(GlobalConstants.PhotoParameters.getFilterName(name), args.getOptionValues(name));
            }
        }
        setNonFilterData();
    }

    /**
     The non-filter data will be set in the class variables but removed from the parameter map as that is used to
     generate the photo service queryString.
     */
    private void setNonFilterData() {
        if (parameters.get(GlobalConstants.PhotoParameters.PHOTOSERVICE_URL.text) == null
            || parameters.get(GlobalConstants.PhotoParameters.PHOTOSERVICE_URL.text).isEmpty()) {
            photosUrl = GlobalConstants.PHOTO_SERVICE_DEFAULT;
        } else {
            photosUrl = parameters.get(GlobalConstants.PhotoParameters.PHOTOSERVICE_URL.text).get(0);
            parameters.remove(GlobalConstants.PhotoParameters.PHOTOSERVICE_URL.text);
        }

        if (parameters.get(GlobalConstants.PhotoParameters.TERMINAL_OUT.text) == null
            || parameters.get(GlobalConstants.PhotoParameters.TERMINAL_OUT.text).isEmpty()) {
            terminalOut = GlobalConstants.TERMINAL_OUT_DEFAULT;
        } else {
            terminalOut = Integer.valueOf(parameters.get(GlobalConstants.PhotoParameters.TERMINAL_OUT.text).get(0));
            parameters.remove(GlobalConstants.PhotoParameters.TERMINAL_OUT.text);
        }

        if (parameters.get(GlobalConstants.PhotoParameters.FILE_URL.text) == null
            || parameters.get(GlobalConstants.PhotoParameters.FILE_URL.text).isEmpty()) {
            fileUrl = GlobalConstants.FILE_URL_DEFAULT;
        } else {
            fileUrl = parameters.get(GlobalConstants.PhotoParameters.FILE_URL.text).get(0);
            parameters.remove(GlobalConstants.PhotoParameters.FILE_URL.text);
        }
    }

    /**
     This calls the photo service with the configured parameters and creates the output to display to the console or
     output file.

     @return console output String
     */
    private String callPhotoService() {
        setFilteredUrl();
        String response = restTemplate.getForEntity(filteredUrl, String.class).getBody();
        Photo[] photos;
        try {
            LOGGER.debug(response);
            photos = objectMapper.readValue(response, Photo[].class);
        } catch (IOException e) {
            LOGGER.error("Failed to marshal response to PhotoList object with Exception: {}, response was: {}",
                e.getMessage(), response);
            throw new InternalError();
        }
        return getStringOutput(photos);
    }

    /**
     This creates the fill service url with queryString filters generated from command line parameter map.
     */
    private void setFilteredUrl() {
        try {
            filteredUrl = UriComponentsBuilder.newInstance().uri(new URI(photosUrl)).queryParams(parameters).build()
                .toUriString();
        } catch (URISyntaxException e) {
            LOGGER.error("Invalid photo service url provided: {}", photosUrl);
            throw new InternalError();
        }

    }

    /**
     This generates the output for the console, additionally writing to an output file if the output is larger than the
     configured terminal limit.

     @param photos photo service response Object
     @return console output String
     */
    private String getStringOutput(Photo[] photos) {
        StringBuilder builder = new StringBuilder();
        if (photos.length == 0) {
            return "No photos were found for the given search: " + filteredUrl;
        }

        for (int x = 0; x < terminalOut && x < photos.length; x++) {
            builder.append(photos[x].toString());
        }
        if (photos.length >= terminalOut) {
            writeResultsToFile(photos);
            builder.append("More than ").append(terminalOut).append(" results returned, full output written to file: ")
                .append(fileUrl);
        }
        return builder.toString();
    }

    /**
     This writes to the configured output file if the response is larger than specified.

     @param photos photo service response Object
     */
    private void writeResultsToFile(Photo[] photos) {
        try {
            FileUtils.writeStringToFile(new File(fileUrl),
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(photos), "UTF-8", false);
        } catch (IOException e) {
            LOGGER.error("Failed writing photos object as string with exception e: {}", e.getMessage());
            throw new InternalError();
        }
    }

    public String getPhotosUrl() {
        return photosUrl;
    }

    public String getAlbumId() {
        return albumId;
    }

    public String getPhotoId() {
        return photoId;
    }

    public String getTitle() {
        return title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public int getTerminalOut() {
        return terminalOut;
    }
}

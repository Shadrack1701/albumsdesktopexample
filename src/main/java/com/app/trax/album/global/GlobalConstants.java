package com.app.trax.album.global;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 Created by mtrachsel on 5/22/2018.
 Purpose:
 */
public final class GlobalConstants {

    public static final int TERMINAL_OUT_DEFAULT = 100;
    public static final String FILE_URL_DEFAULT = "c:/PhotoRunnerOutput/output.json";
    public static final String PHOTO_SERVICE_DEFAULT = "https://jsonplaceholder.typicode.com/photos";

    private GlobalConstants() {
    }

    /**
     Specifies valid command line arguments.
     */
    public enum PhotoParameters {
        ALBUM_ID("albumid", "albumId"),
        PHOTO_ID("photoid", "id"),
        TITLE("title", "title"),
        PHOTO_URL("photourl", "url"),
        THUMBNAIL_URL("thumburl", "thumbnailUrl"),
        PHOTOSERVICE_URL("photoserviceurl", "photoserviceurl"),
        FILE_URL("fileurl", "fileurl"),
        TERMINAL_OUT("terminalout", "terminalout");

        private static final Map<String, PhotoParameters> enumMap = new HashMap<>();

        static {
            for (PhotoParameters value : EnumSet.allOf(PhotoParameters.class)) {
                enumMap.put(value.text, value);
            }
        }

        public final String text;
        public final String filterName;

        PhotoParameters(final String text, final String filterName) {
            this.text = text;
            this.filterName = filterName;
        }

        public static boolean isValidPhotoParameters(String parameter) {
            return PhotoParameters.enumMap.get(parameter.toLowerCase()) != null;
        }

        public static String getFilterName(String text) {
            return enumMap.get(text.toLowerCase()).filterName;
        }
    }

}
